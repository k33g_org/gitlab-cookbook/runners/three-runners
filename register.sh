#!/bin/bash

set -o allexport; source .env; set +o allexport

sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "${RUNNER_01_DESCRIPTION}" \
  --tag-list "${RUNNER_01_DESCRIPTION}" \
  --run-untagged="true" \
  --locked="false"

sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "${RUNNER_02_DESCRIPTION}" \
  --tag-list "${RUNNER_02_DESCRIPTION}" \
  --run-untagged="true" \
  --locked="false"

sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "${RUNNER_03_DESCRIPTION}" \
  --tag-list "${RUNNER_03_DESCRIPTION}" \
  --run-untagged="true" \
  --locked="false"


