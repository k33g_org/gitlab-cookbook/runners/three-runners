const axios = require("axios")
require('dotenv').config({ path: './.env' });

const gitlabAPI = process.env.GITLAB_API
const groupId = process.env.GROUP_ID
const runner01Description = process.env.RUNNER_01_DESCRIPTION
const runner02Description = process.env.RUNNER_02_DESCRIPTION
const runner03Description = process.env.RUNNER_03_DESCRIPTION

let headers = {
  "Content-Type": "application/json",
  "Private-Token": process.env.GITLAB_TOKEN_ADMIN
}

let query = ({method, path, headers, data}) => {
  return axios({
    method: method,
    url: `${gitlabAPI}/${path}`,
    headers: headers,
    data: data !== null ? JSON.stringify(data) : null
  })
}

let runnersList = ({groupId, headers}) => query({
  method: "GET", 
  path: `groups/${groupId}/runners`,
  headers: headers
})

let removeRunner = ({runnerId, headers}) => query({
  method: "DELETE", 
  path: `runners/${runnerId}`,
  headers: headers
})

runnersList({
  groupId: groupId,
  headers: headers
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
  // Remove all runners (of the group) with a specific description
  runners.filter(runner => runner.description==runner01Description).forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description==runner02Description).forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description==runner03Description).forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})



