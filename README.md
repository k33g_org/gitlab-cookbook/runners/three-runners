# Three Runners

**The runner is registered 3 times, but it use the same executor (no // jobs)**

🖐️ `group_id=14025022` (this is the id of the parent group **"Runners"**)

This runner uses the CI token of its parent group: https://gitlab.com/k33g_org/gitlab-cookbook/runners named **Runners**.

This runner is a group runner

For convenience (and security) reasons I copied the value of the CI **registration** token of the group in an environment variable of my **[Gitpod](https://gitpod.io/variables)** profile: `CICD_TOKEN_COOKBOOK_RUNNERS` (🖐️ if you change the value of this variable, don't forget to reload your Gitpod workspace)

Btw, I registered my admin token in the same way (`GITLAB_TOKEN_ADMIN`)

I use this GitLab url: https://gitlab.com/ 

You can find the token and the GitLab url here: https://gitlab.com/groups/k33g_org/gitlab-cookbook/runners/-/settings/ci_cd (CI/CD Settings section of the **Runners**)

## Prerequisites

Update the `.env` file:

```bash
GITLAB_URL="https://gitlab.com/"
GITLAB_API="https://gitlab.com/api/v4"
GROUP_ID=14025022
RUNNER_01_DESCRIPTION="gitpod_01"
RUNNER_02_DESCRIPTION="gitpod_02"
RUNNER_03_DESCRIPTION="gitpod_03"
```

🖐️ **Deactivate the share runners of the project**

## Refs

- https://docs.gitlab.com/runner/install/
  - https://docs.gitlab.com/runner/install/linux-repository.html
- https://docs.gitlab.com/runner/register/


## Install and register a runner

### Install (Ubuntu)

- https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner

```bash
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"

sudo dpkg -i gitlab-runner_amd64.deb
```

🖐️ You don't need to install the runner if you opened this project from Gitpod (see `.gitpod.yml`)


### Register

- https://docs.gitlab.com/runner/register/#one-line-registration-command

```bash
set -o allexport; source .env; set +o allexport

sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "${RUNNER_DESCRIPTION}" \
  --tag-list "docker,gitpod" \
  --run-untagged="true" \
  --locked="false"

```

**Check**: you should get the record of the runner here: https://gitlab.com/groups/k33g_org/gitlab-cookbook/runners/-/settings/ci_cd

## Test it

Create a `.gitlab-ci.yml` file with this content:

```yaml
stages:
  - greetings

hello:
  stage: greetings
  script:
    - echo "👋 hello world 🌍"
```

Commit (or runt it from GitLab)

## Remove a runner with the API

Use: `node remove.one.runner.js`

### With Curl

```bash
# get list of group's runners
group_id=14025022
curl --request GET --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/groups/${group_id}/runners"
```

```bash
# delete runner by id
runner_id=11994540
curl --request DELETE --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/runners/${runner_id}"
```




